Gaming with EaselJs


### Install ###
Install node and npm.

Then;
```
npm install -g bower
npm install -g grunt
```
Then from the specific lesson folder you want to run
```
bower install
npm install
```

### Run ###
Also from the lesson folder
```
grunt
```

navigate to localhost:9001 in a modern browser
